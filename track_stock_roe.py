#!/usr/bin/env python3
import requests
import re
from bs4 import BeautifulSoup
import datetime
import json
import logging
import time
from collections import deque 
from lxml import etree
from io import StringIO
import random
from stock_ids import stock_ids
from tabulate import tabulate

logging.basicConfig(
    # filename='HISTORYlistener.log',
    level=logging.INFO,
    format='%(asctime)s.%(msecs)03d %(levelname)07s %(module)s - %(funcName)s: %(message)s',
    datefmt='%y-%m-%d %H:%M:%S',
    handlers=[
        logging.StreamHandler()
    ]
)
info=logging.info

def extract_url(url):

    if url.find("www.amazon.in") != -1:
        index = url.find("/dp/")
        if index != -1:
            index2 = index + 14
            url = "https://www.amazon.in" + url[index:index2]
        else:
            index = url.find("/gp/")
            if index != -1:
                index2 = index + 22
                url = "https://www.amazon.in" + url[index:index2]
            else:
                url = None
    else:
        url = None
    return url

def getIndex(s, i): 
  
    # If input is invalid. 
    if s[i] != '{': 
        return -1
  
    # Create a deque to use it as a stack. 
    d = deque() 
  
    # Traverse through all elements 
    # starting from i. 
    for k in range(i, len(s)): 
  
        # Pop a starting bracket 
        # for every closing bracket 
        if s[k] == '}': 
            d.popleft() 
  
        # Push all starting brackets 
        elif s[k] == '{': 
            d.append(s[i]) 
  
        # If deque becomes empty 
        if not d: 
            return k 
  
    return -1

class stock_info:
    def __init__(self):
        self.roe=None
        self.roa=None
        self.net_value=None
        self.dividend_year=None
        self.dividend=None

def get_yahoo_stock_info(stock_id):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
        # "Cookie": "cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
        # "Cookie": "stock_user_uuid=f0d5b6ba-a7c4-40ae-afff-f7ad83e248c0; stock_id=paf78801; cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
    }
    url="https://tw.stock.yahoo.com/d/s/dividend_"+stock_id+".html"
    session = requests.Session()
    response = session.get(url, headers=headers)
    response.encoding="utf8"
    # info(response.text)
    soup = BeautifulSoup(response.text, features="lxml")
    lastest_dividend_tr=soup.select_one("body table:nth-child(2) tr:nth-child(2)")
    lastest_dividend_year=lastest_dividend_tr.select_one("td:nth-child(1)").contents[0]
    lastest_dividend_value=lastest_dividend_tr.select_one("td:nth-child(2)").contents[0]
    # info("lastest_dividend=", lastest_dividend_year, lastest_dividend_value)
    ret = stock_info()
    ret.dividend_year = lastest_dividend_year
    ret.dividend = lastest_dividend_value
    # ret["net_value"]=net_value
    # ret["roe"]=roe
    # info(ret)
    return ret

def get_numbers_in_text(text):
    return re.findall(r"[-+]?\d*\.\d+|\d+", text)[0]

def get_good_stock_performance_info(stock_id, ret_dict):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
        # "Cookie": "cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
        # "Cookie": "stock_user_uuid=f0d5b6ba-a7c4-40ae-afff-f7ad83e248c0; stock_id=paf78801; cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
    }
#    url = "https://goodinfo.tw/StockInfo/StockBzPerformance.asp?STOCK_ID="+stock_id+"&RPT_CAT=M%5FYEAR"
    url = "https://goodinfo.tw/StockInfo/StockBzPerformance.asp?STOCK_ID="+stock_id+"&YEAR_PERIOD=10&RPT_CAT=M_YEAR"
    info("query url="+url)
    session = requests.Session()
    response = session.get(url, headers=headers)
    response.encoding="utf8"
    # info(response.text)
    soup = BeautifulSoup(response.text, features="lxml")
    # financial_score = soup.select("a[title='最近一個財報年度的評分']")[0].select_one().contents[0]
    # "span[title='歷年各年度財報的平均分數']"
    # financial_score_latest = get_numbers_in_text(financial_score.select_one("a").contents[0])
    # financial_score_avg = get_numbers_in_text(financial_score.select_one("span").contents[0])
    financial_score_latest = soup.select_one("a[title='最近一個財報年度的評分']").contents[0]
    financial_score_avg = soup.select_one("span[title='歷年各年度財報的平均分數']").contents[0]
    info(financial_score_latest)
    info(financial_score_avg)
    fin_avg_data=soup.select('#txtFinAvgData')[0]
    # info(fin_avg_data)

    roe=fin_avg_data.select_one('tr[bgcolor=white] +tr+tr+tr+tr td:nth-child(2)').contents[0]
    roa=fin_avg_data.select_one('tr[bgcolor=white] +tr+tr+tr+tr td:nth-child(4)').contents[0]
    info('roe='+str(roe))
    info('roa='+str(roa))
    net_value = fin_avg_data.select_one('tr:nth-child(5) > td:nth-child(1) > nobr:nth-child(3)').contents[0]
    net_value = get_numbers_in_text(net_value)
    info('net_value='+str(net_value))
    # ret = dict()
    ret_dict['net_value']=net_value
    ret_dict['roe']=roe
    ret_dict['roa']=roa
    ret_dict['financial_score_latest']=financial_score_latest
    ret_dict['financial_score_avg']=financial_score_avg
    # info(select_items[4])
    # return ret

def get_good_stock_dividend_info(stock_id, ret_dict):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
        # "Cookie": "cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
        # "Cookie": "stock_user_uuid=f0d5b6ba-a7c4-40ae-afff-f7ad83e248c0; stock_id=paf78801; cookie=WVRveU9udHpPakk2SW1sMklqdHpPakUyT2lJQldvaFBDSzNSdDBJcVViYkhqamJsSWp0ek9qVTZJbU55ZVhCMElqdHpPalkwT2lJUmw3U2VXUUNRM0RxUEhqb2RSQl92cHRwcl80bmZUNV9WX1RYSEdtMFZ0OThfb1lDQmNnWTFNTC9YZVBmbU5YN19rM2RCSE5mY3ZDVllaWjdFNGloUklqdDk%3D; stockid_records_flag=1; stock_popup_stocknews=1; stockid_records=YToyOntpOjA7aToyMzU3O2k6MTtpOjM3MDI7fQ%3D%3D; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MTtzOjQ6InR5cGUiO2k6MDt9"
    }
#    url = "https://goodinfo.tw/StockInfo/StockBzPerformance.asp?STOCK_ID="+stock_id+"&RPT_CAT=M%5FYEAR"
    url = "https://goodinfo.tw/StockInfo/StockDividendPolicy.asp?STOCK_ID="+stock_id
    info("query url="+url)
    session = requests.Session()
    response = session.get(url, headers=headers)
    response.encoding="utf8"
    # info(response.encoding)
    soup = BeautifulSoup(response.text, features="lxml")
    latest_dividend_tr=soup.select_one("#tblDetail")
    latest_dividend_year = latest_dividend_tr.select_one("tr:nth-child(5) > td:nth-child(1) > nobr > b").contents[0]
    info("latest_dividend_year="+str(latest_dividend_year))
    latest_dividend_value = latest_dividend_tr.select_one("td:nth-child(8)").contents[0]
    info("latest_dividend_value="+str(latest_dividend_value))
    # ret = dict()
    ret_dict['dividend_year']=latest_dividend_year
    ret_dict['dividend']=latest_dividend_value
    # info(select_items[4])
    # return ret

dividend_set=None
closing_price_set=None

def get_dividend_yield():
    global dividend_set
    url = "https://openapi.twse.com.tw/v1/exchangeReport/BWIBBU_ALL"
    response = requests.get(url)
    json_loaded = json.loads(response.text)
    # print(json_loaded[0])
    # ret_dict['dividend']=json_loaded[0].DividendYield
    dividend_set = json_loaded

def get_closing_price():
    global closing_price_set
    url = "https://openapi.twse.com.tw/v1/exchangeReport/STOCK_DAY_AVG_ALL"
    response = requests.get(url)
    json_loaded = json.loads(response.text)
    # print(json_loaded[0])
    # ret_dict['price']=json_loaded[0].ClosingPrice
    closing_price_set = json_loaded

def query_price_dividend_by_id(stock_code, ret_dict):
    # global dividend_set
    # for dividend_info in dividend_set:
    #     if dividend_info['Code'] == stock_code:
    #         info(dividend_info)
    #         ret_dict['dividend']=dividend_info['DividendYield']
    global closing_price_set
    for price_info in closing_price_set:
        if price_info['Code'] == stock_code:
            info(price_info)    
            ret_dict['price']=price_info['ClosingPrice']

def all_stock_info():
    """
1101
1102
    """
    global stock_ids
    stocks=stock_ids.split('\n')
    final=[]
    # get_dividend_yield()
    get_closing_price()
    # query_price_dividend_by_id('2379')
    # return
    for s in stocks:
        s=s.strip()
        if(len(s)>0):
            good_ret=None
            yahoo_ret=None
            info("get_stock_info "+s)
            ret_dict=dict()
            query_price_dividend_by_id(s, ret_dict)
            if(s.startswith("00")):
                good_ret=None
            else:
                time.sleep(random.randint(30, 60))
                get_good_stock_performance_info(s, ret_dict)
                # yahoo_ret = get_yahoo_stock_info(s)
            # good_ret = dict()
            # if(good_ret is not None):
                time.sleep(random.randint(30, 60))
                get_good_stock_dividend_info(s, ret_dict)
                # info(good_ret)
                final.append([s, ret_dict['price'], ret_dict['roe'], ret_dict['roa'], ret_dict['net_value'], ret_dict['financial_score_latest'], ret_dict['financial_score_avg'], 
                ret_dict['dividend_year'], ret_dict['dividend']])
                # info("{0}\t{1}\t{2}".format(s, ret[0], ret[1]))
            # else:
            #     final.append('')
        else:
            final.append('')
    header=["code", "price", "roe", "roa", "netvalue", "financial_score_latest", "financial_score_avg", "dividend_year", "dividend"]    
    print(tabulate(final, headers=header))
    
    # for f in final:
    #     print(f)
if __name__ == "__main__":
    all_stock_info()
